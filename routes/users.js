var express = require('express');
var router = express.Router();
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });
var mammoth = require("mammoth");
var fs = require('fs-extra');
var path = require('path');

// Imports the Google Cloud client library
const textToSpeech = require('@google-cloud/text-to-speech');
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('show', { title: 'Text to Speech Converter' });
});
router.post('/upload', upload.single('docfile'), function(req, res, next) {
  mammoth.extractRawText({path: req.file.path})
    .then(function(result){
        var inputtext = result.value; // The raw text
       // Creates a client
const client = new textToSpeech.TextToSpeechClient({
    keyFilename: ''
  });

// The text to synthesize
const text = inputtext;

// Construct the request
const request = {
  input: {text: text},
  // Select the language and SSML Voice Gender (optional)
  voice: {languageCode: 'en-US', ssmlGender: 'NEUTRAL'},
  // Select the type of audio encoding
  audioConfig: {audioEncoding: 'MP3'},
};

// Performs the Text-to-Speech request
client.synthesizeSpeech(request, (err, response) => {
  if (err) {
    console.error('ERROR:', err);
    return;
  }

  const file = './public/output/output.mp3'
  // Write the binary audio content to a local file
  fs.outputFile(file, response.audioContent, 'binary', err => {
    if (err) {
      console.error('ERROR:', err);
      return;
    }
    else{
    console.log('Audio content written to file: output.mp3');
    res.redirect("/users");
  }
  });
});     
})
.done();
//res.redirect("/users");
 
});

module.exports = router;
